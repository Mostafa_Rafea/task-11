import React from 'react';
import Login from './components/Login';
import Email from './components/email';
import Inbox from './components/inbox';
import Sent from './components/sent';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route, Link, Redirect } from "react-router-dom";

function NavigationBar() {
    return (
        <Navbar bg="dark" variant="dark">
            <Container>
                <Navbar.Brand>Company</Navbar.Brand>
                <Nav className="mr-auto">
                    <Link to="/email" className="nav-link">New Message</Link>
                    <Link to="/inbox" className="nav-link">Inbox</Link>
                    <Link to="/sent" className="nav-link">Sent</Link>
                </Nav>
                <Nav>
                    <Nav.Link href="#profile">Profile</Nav.Link>
                    <Link to="/" className="nav-link">Log Out</Link>
                </Nav>
            </Container>
        </Navbar>
    )
}

class App extends React.Component {

    State = {
        users: [],
        sent: [],
        messages: [],
        isLoggedIn: false
    }

    input1 = React.createRef();
    input2 = React.createRef();

    to = React.createRef();
    subject = React.createRef();
    message = React.createRef();

    // Login Button in Login Page

    login = (e) => {
        e.preventDefault();
        let newUser = {
            name: this.input1.current.value,
            email: this.input2.current.value
        }
        let users = this.State.users;
        users.push(newUser);
        this.setState({ users });
        this.setState({ isLoggedIn: true });
    }

    isLoggedIn = () => {
        return this.State.isLoggedIn;
    }

    // Send Button

    send = (e) => {
        e.preventDefault();
        let sent = this.State.sent;
        let newMessage = {
            id: sent.length,
            name: this.state.users[this.state.users.length - 1].name,
            email: this.to.current.value,
            subject: this.subject.current.value
        }
        sent.push(newMessage);
        this.setState({ sent });

        let messages = this.State.messages;
        let message = {
            id: messages.length,
            name: this.state.users[this.state.users.length - 1].name,
            email: this.state.users[this.state.users.length - 1].email,
            subject: this.subject.current.value,
            status: false
        }
        messages.push(message);
        this.setState({ messages });
    }

    // Cancel Button

    cancel = () => {
        this.to.current.value = "";
        this.subject.current.value = "";
        this.message.current.value = "";
    }

    // Change Status in inbox

    status = (id) => {
        let messages = this.State.messages;
        messages.filter((item) => {
            if (id === item.id) {
                item.status = true;
            }
        })
    }

    render() {
        return (
            <React.Fragment>
                <Router path="/">
                    <Switch>
                        <Route path="/email">
                            <NavigationBar />
                            <Email ref1={this.to} ref2={this.subject} ref3={this.message} send={this.send} cancel={this.cancel} />
                        </Route>
                        <Route path="/inbox">
                            <NavigationBar />
                            <Inbox messages={this.State.messages} status={this.status} />
                        </Route>
                        <Route path="/sent">
                            <NavigationBar />
                            <Sent sent={this.State.sent} />
                        </Route>
                        <Route exact path="/">
                            <Login ref1={this.input1} ref2={this.input2} login={this.login} />
                        </Route>
                    </Switch>
                </Router>
            </React.Fragment>
        )
    }
}

export default App;
