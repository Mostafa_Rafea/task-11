import React from 'react';
import ReactDOM from 'react-dom';
import App from './app';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/base.css';

ReactDOM.render(<App />, document.getElementById('root'));

