import React, { Component } from "react";
import { Button, Form, InputGroup } from "react-bootstrap";
import { withRouter } from 'react-router-dom';

class Login extends Component {

    routeChange = (e) => {
        e.preventDefault();
        let path = `/email`;
        this.props.history.push(path);
    }

    render() {
        return (
            <div className="Login">
                <form className="d-flex justify-content-center align-items-center flex-column">
                    <Form.Group controlId="name" className="w-100">
                        <Form.Group controlId="formBasicName">
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text id="name">Name</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    autoFocus
                                    ref={this.props.ref1}
                                    placeholder="Enter your name"
                                    aria-label="name"
                                    aria-describedby="name"
                                />
                            </InputGroup>
                        </Form.Group>
                    </Form.Group>
                    <Form.Group controlId="email" className="w-100">
                        <Form.Group controlId="formBasicEmail">
                            <InputGroup className="mb-3">
                                <InputGroup.Prepend>
                                    <InputGroup.Text id="email">Email</InputGroup.Text>
                                </InputGroup.Prepend>
                                <Form.Control
                                    ref={this.props.ref2}
                                    placeholder="Enter your email"
                                    aria-label="email"
                                    aria-describedby="email"
                                />
                            </InputGroup>

                        </Form.Group>
                    </Form.Group>
                    <Button block className="w-50" type="submit" onClick={(e) => { this.props.login(e); this.routeChange(e); }}>
                        Login
                    </Button>
                </form>
            </div>
        );
    }
}

export default withRouter(Login);