import React from 'react';
import { Container, Table } from 'react-bootstrap';

class Sent extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Container className="my-5 inbox px-0">
                    <h6 className="p-3">Sent</h6>
                    <p className="px-3">Check out the messages you sent !!</p>
                    <Table striped hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Subject</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.sent.map((item) => {
                                return (
                                    <tr>
                                        <td>{item.id}</td>
                                        <td>{item.name}</td>
                                        <td>{item.email}</td>
                                        <td>{item.subject}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table>
                </Container>
            </React.Fragment>
        )
    }
}

export default Sent;