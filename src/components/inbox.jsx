import React from 'react';
import { Container, Table } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSquare, faCheckSquare } from '@fortawesome/free-solid-svg-icons'

class Inbox extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Container className="my-5 inbox px-0">
                    <h6 className="p-3">Inbox</h6>
                    <p className="px-3">Check out your messages !!</p>
                    <Table striped hover>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Subject</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.messages.map((item) => {
                                return (
                                    <tr onClick={() => { this.props.status(item.id) }}>
                                        <td>{item.id}</td>
                                        <td>{item.name}</td>
                                        <td>{item.email}</td>
                                        <td>{item.subject}</td>
                                        <td>
                                            <FontAwesomeIcon icon={item.status ? faCheckSquare : faSquare} />
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </Table>
                </Container>
            </React.Fragment>
        )
    }
}

export default Inbox;