import React from 'react';
import { Form, Button, Container } from 'react-bootstrap';

class Email extends React.Component {
    render() {
        return (
            <React.Fragment>
                <Form className="my-5">
                    <Container className="px-0">
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>To</Form.Label>
                            <Form.Control ref={this.props.ref1} type="email" placeholder="Enter email" />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Subject</Form.Label>
                            <Form.Control ref={this.props.ref2} type="text" />
                        </Form.Group>
                        <Form.Group controlId="exampleForm.ControlTextarea1" className="mb-5">
                            <Form.Label>Message</Form.Label>
                            <Form.Control ref={this.props.ref3} as="textarea" rows="10" />
                        </Form.Group>
                        <Button variant="primary" className="bg-submit px-5" type="submit" onClick={this.props.send}>
                            Send
                        </Button>
                        <Button variant="light" className="ml-2 px-5 cancel" type="button" onClick={this.props.cancel}>
                            Cancel
                        </Button>
                    </Container>
                </Form>
            </React.Fragment>
        )
    }
}

export default Email;